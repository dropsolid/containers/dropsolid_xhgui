<?php

return [
  'save.handler' => 'pdo',
  'pdo' => [
    'dsn' => "sqlite:/data/db/xhgui.sqlite3",
    'user' => null,
    'pass' => null,
    'table' => 'results',
    'tableWatch' => 'watches',
  ],
  'upload.token' => 'xhgui',
  'date.format' => 'd/m/Y H:i:s',
  'detail.count' => 6,
  'page.limit' => 25,
  'timezone' => 'Europe/Brussels',
];
