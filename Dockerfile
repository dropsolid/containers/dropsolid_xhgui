FROM php:8.2-alpine as builder

ENV XHGUI_VERSION 0.21.3
ENV PROFILER_VERSION 1.1.1

RUN apk add --no-cache tzdata

RUN wget -c https://github.com/perftools/xhgui/archive/refs/tags/${XHGUI_VERSION}.tar.gz; \
    tar zxvf ${XHGUI_VERSION}.tar.gz; \
    cd xhgui-${XHGUI_VERSION}; \
    php install.php; \
    php composer.phar install --no-dev; \
    php composer.phar update slim/slim --no-dev;

RUN wget -c https://github.com/perftools/php-profiler/archive/refs/tags/${PROFILER_VERSION}.tar.gz; \
    tar zxvf ${PROFILER_VERSION}.tar.gz;

RUN mkdir /xhgui; \
    cd /xhgui-${XHGUI_VERSION}; \
    mv cache config external src vendor webroot templates /xhgui; \
    mkdir /xhgui/php-profiler; \
    cd /php-profiler-${PROFILER_VERSION}; \
    mv config src autoload.php /xhgui/php-profiler;

FROM php:8.2-alpine

COPY --from=builder /usr/share/zoneinfo/Europe/Brussels /etc/localtime
RUN echo "Europe/Brussels" > /etc/timezone

COPY php.ini /usr/local/etc/php/php.ini

COPY --from=builder /xhgui /xhgui
COPY config.default.php /xhgui/config/config.php
COPY prepend.php /xhgui/external/header.php

RUN mkdir -p /data/db

EXPOSE 8080 

VOLUME /xhgui

COPY run.sh /run.sh
RUN chmod 755 /run.sh

CMD /run.sh
