<?php

require_once '/xhgui/php-profiler/autoload.php';

$config = [
  'save.handler' => \Xhgui\Profiler\Profiler::SAVER_UPLOAD,
  'save.handler.upload' => [
    'url' => 'http://xhgui.' .  getenv('DOCKER_NETWORK') . ':8080/run/import',
    'token' => 'xhgui',
  ],
  'profiler.enable' => function() {
    return (
      !empty($_ENV['XHPROF_ENABLE']) ||
      !empty($_GET['XHPROF_ENABLE']) ||
      !empty($_COOKIE['XHPROF_ENABLE'])
    );
  },
];

if (file_exists('/var/www/html/etc/dropsolid_xhgui_profiler_config.php')) {
  require_once('/var/www/html/etc/dropsolid_xhgui_profiler_config.php');
}

$profiler = new \Xhgui\Profiler\Profiler($config);
$profiler->start();
